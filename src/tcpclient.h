#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include "tcp.h"
#include <stdio.h>

#define DISCONNECTED 0
#define CONNECTED_TO_SERVER 1

typedef struct
{
    int sock; // oponent socket
    struct sockaddr_in address; // definice příchozích adres a protokolů
    char connected; // stav socketů
} SOCK_TCP_Client;

/*
 * Allocate and set up SOCK_TCP_Client structure.
 *
 * Return:
 *  On success: address of allocated structure
 *  On fail: NULL
 */
SOCK_TCP_Client* SOCK_TCP_Client_Start();

/*
 * Client will try to connect to server.
 *
 * Parametrs:
 *  SOCK_TCP_Client* the_client - allocated server structure
 *  const char* ip - server ip address
 *  unsigned short port - server port
 *
 * Return:
 *  On success: 0
 *  On fail: 1
 */
int SOCK_TCP_Client_ConnectToServer(SOCK_TCP_Client* the_client, const char* ip, unsigned short port);

/*
 * Shutdown the connection.
 */
void SOCK_TCP_Client_LeaveServer(SOCK_TCP_Client* ToLeave);

/*
 * Shutdown the connection and free SOCK_TCP_Client structure.
 */
void SOCK_TCP_Client_Stop(SOCK_TCP_Client* ToStop);

#endif // TCPSERVER_H
