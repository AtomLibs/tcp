#include "tcpclient.h"

SOCK_TCP_Client* SOCK_TCP_Client_Start()
{
  SOCK_TCP_Client* ret = (SOCK_TCP_Client*)malloc(sizeof(SOCK_TCP_Client));
  if (ret == NULL) return NULL;
  ret->connected = DISCONNECTED;
  return ret;
}

int SOCK_TCP_Client_ConnectToServer(SOCK_TCP_Client* the_client, const char* ip, unsigned short port)
{
  the_client->sock = socket(AF_INET, SOCK_STREAM, 0);
  if (the_client->sock < 0) return ERROR;
  the_client->address.sin_family = AF_INET;
  // TODO: Check if ip format is right
  the_client->address.sin_addr.s_addr = inet_addr(ip);
  the_client->address.sin_port = htons(port);
  if (connect(the_client->sock, (struct sockaddr*)&the_client->address, sizeof(the_client->address)) < 0)
  {
    SOCK_CloseSocket(the_client->sock);
    return ERROR;
  }
  the_client->connected = CONNECTED_TO_SERVER;
  return EOK;
}

void SOCK_TCP_Client_LeaveServer(SOCK_TCP_Client* ToLeave)
{
  SOCK_CloseSocket(ToLeave->sock);
  ToLeave->connected = DISCONNECTED;
}

void SOCK_TCP_Client_Stop(SOCK_TCP_Client* ToStop)
{
  SOCK_CloseSocket(ToStop->sock);
  free(ToStop);
}
