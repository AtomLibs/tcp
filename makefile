GPP = gcc
SOURCES = $(wildcard src/*.c tests/main-test.c)
OBJ = $(patsubst %.c,obj/%.o,$(SOURCES))
OBJDIRS = $(dir $(OBJ))
CFLAGS = -g

.PHONY: clean build

$(shell mkdir -p $(OBJDIRS))

main-test: $(OBJ)
	$(GPP) $(CFLAGS) $^ -o $@

obj/%.o: %.c
	$(GPP) -c -o $@ $^

clean:
	rm -rf obj
