#include <stdio.h>

#include "../src/tcpserver.h"
#include "../src/tcpclient.h"

int main(int argc, char* argv[])
{
  int choice;
  scanf("%d\n", &choice);
  char buffer[512];
  switch (choice) {
    case 0: ;
      printf("Starting server.\n");
      SOCK_TCP_Server *theServer = SOCK_TCP_Server_Start(8080, 1, 1);
      printf("Starting server.\n");
      SOCK_TCP_Server_AcceptClient(theServer);
      printf("Starting server.\n");
      char ip[16];
      printf("Starting server.\n");
      unsigned short port;
      printf("Starting server.\n");
      printf("%d\n", SOCK_GetOtherSideInfo(ip, &port, theServer->clients[0].sock, &theServer->clients[0].address, theServer->clients->addrlen));
      printf("New connection from: %s form port: %d\n", ip, port);
      SOCK_SendDataTimeout(theServer->clients[0].sock, "ahoj!", sizeof("ahoj!"), 10, 1);
      printf("Reading data.\n");
      SOCK_ReadData(theServer->clients[0].sock, buffer, sizeof(buffer));
      printf("Received: %s\n", buffer);
      SOCK_TCP_Server_Stop(theServer);
      break;
    case 1: ;
      printf("Starting client.\n");
      SOCK_TCP_Client *theClient = SOCK_TCP_Client_Start();
      printf("Connecting to server.\n");
      SOCK_TCP_Client_ConnectToServer(theClient, "127.0.0.1", 8080);
      printf("Reading data.\n");
      SOCK_ReadData(theClient->sock, buffer, sizeof(buffer));
      printf("Received: %s\n", buffer);
      printf("Sending...\n");
      SOCK_SendDataTimeout(theClient->sock, "diky!!!", sizeof("diky!!!"), 10, 1);
      SOCK_TCP_Client_Stop(theClient);
      break;
    default: ;
      return 0;
      break;
  }
  return 0;
}
